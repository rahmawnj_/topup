<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Logs extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library(["form_validation", 'session', 'encryption']);
        $this->load->model(['log_model', 'setting_model']);
        $this->load->helper(['form', 'url']);
        if (!$this->session->userdata('status')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible" role="alert">
            <div class="alert-message">
            Login terlebih dahulu!
            </div>
        </div>');
            redirect('auth/login');
        }
    }

    public function index()
    {
        if ($this->input->get('date')) {
            $printlog = $this->log_model->get_log_by_date($this->input->get('date'));
            $logs = $this->log_model->get_log_by_date($this->input->get('date'));
            $tanggal = $this->input->get('date');
        } else {
            $logs = $this->log_model->get_logs();
            $printlog = [];
            $tanggal = '';
        }
   
        $data = [
            'title' => 'Transaksi Topup',
            'logs' => $logs,
            'print_logs' => $printlog,
            'tanggal' => $tanggal,
            'settings' => $this->setting_model->get_settings()

        ];

        $this->load->view('log/index', $data);
    }


    public function view($id_log)
    {
        $data = [
            'title' => 'Transaksi Topup',
            'logs' => $this->log_model->get_log('id_log', $id_log)[0],
        ];
        $this->load->view('log/view', $data);
    }

    public function edit($id_log)
    {
        $data = [
            'title' => 'Transaksi Topup',
            'log' => $this->log_model->get_log('id_log', $id_log)[0]
        ];
        $this->load->view('log/edit', $data);
    }
    public function update()
    {

        $this->form_validation->set_rules('status', 'Status', 'required');

        if ($this->form_validation->run() == false) {
            $this->edit($this->input->post('id_log'));
        } else {

            $this->log_model->update($this->input->post('id_log'), [
                'status' => $this->input->post('status'),
                'updated_at' => date('Y-m-d H:i:s')
            ]);

            $this->session->set_flashdata('success', 'Status Topup Berhasil Diubah');
            redirect('/logs');
        }
    }

    public function print($id_log)
    {
        $data = [
            'title' => 'Transaksi Topup',
            'log' => $this->log_model->get_log('id_log', $id_log)[0]
        ];
        $this->load->view('log/print', $data);
    }
}
