<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Devices extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library("form_validation");
        $this->load->model(['device_model', 'setting_model', 'log_model']);
        $this->load->helper(['form', 'url']);
        if (!$this->session->userdata('status')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible" role="alert">
            <div class="alert-message">
            Login terlebih dahulu!
            </div>
        </div>');
            redirect('auth/login');
        }
    }

    public function index()
    {
        $data = [
            'title' => 'Devices',
            'devices' => $this->device_model->get_devices()
        ];
        $this->load->view('/devices/index', $data);
    }

    public function dump()
    {
        $data = [
            'title' => 'Dump Devices',
            'devices' => $this->device_model->get_device('deleted', '1')
        ];
        $this->load->view('/devices/dump', $data);
    }

    public function create()
    {
        $data = [
            'title' => 'Devices',
        ];
        $this->load->view('/devices/create', $data);
    }

    public function store()
    {
        $this->form_validation->set_rules('name', 'Device', 'required|trim');
        $this->form_validation->set_rules('code', 'ID Device', 'required|trim|is_unique[devices.code]');
        if ($this->form_validation->run() == false) {
            $this->create();
        } else {
            $this->device_model->insert([
                'code' => $this->input->post('code'),
                'name' => $this->input->post('name'),
                'deleted' => 0,
                'created_at' => date('Y-m-d H:i:s')
            ]);
            $this->session->set_flashdata('success', 'Devices Berhasil Ditambahkan!');
            redirect('/devices');
        }
    }

    public function view($id_device)
    {
        $data = [
            'title' => 'Devices',
            'device' => $this->device_model->get_device('id_device', $id_device)[0]
        ];
        $this->load->view('/devices/view', $data);
    }

    public function edit($id_device)
    {
        $data = [
            'title' => 'Devices',
            'device' => $this->device_model->get_device('id_device', $id_device)[0]
        ];
        $this->load->view('/devices/edit', $data);
    }

    public function restore($id_device)
    {
        $this->device_model->update($id_device, [
            'deleted' => 0,
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        $this->session->set_flashdata('success', 'Device berhasil direstore!');
        redirect('/devices/dump');
    }

    public function update()
    {
        $device = $this->device_model->get_device('id_device', $this->input->post('id_device'))[0];
        
        if ($this->input->post('code') == $device['code']) {
            $device_rules = 'required|trim';
        } else {
            $device_rules = 'required|trim|is_unique[devices.code]';
        }
        $this->form_validation->set_rules('code', 'ID Device', $device_rules);
        $this->form_validation->set_rules('name', 'Device', 'required|trim');
        if ($this->form_validation->run() == false) {
            $this->edit($this->input->post('id_device'));
        } else {
            
            $this->device_model->update($this->input->post('id_device'), [
                'code' => $this->input->post('code'),
                'name' => $this->input->post('name'),
                'updated_at' => date('Y-m-d H:i:s')
            ]);
            $this->session->set_flashdata('success', 'Devices Berhasil Diperbarui!');
            redirect('/devices');
        }
    }

    public function delete($id_device)
    {
        $this->device_model->delete('soft_delete', $id_device);
        $this->session->set_flashdata('success', 'Devices Berhasil Dihapus!');
        redirect('/devices');
    }

    public function dump_delete($id_device)
    {
        $logs = $this->log_model->get_log('device', $id_device);
        foreach ($logs as $key => $log) {
            $this->log_model->delete($log['id_log']);
        }

        $this->device_model->delete('permanent', $id_device);
        $this->session->set_flashdata('success', 'Devices Berhasil Dihapus!');
        redirect('/devices/dump');
    }
}
