<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Setting extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library("form_validation");
        $this->load->model(['setting_model']);
        $this->load->helper(['form', 'url']);
        if (!$this->session->userdata('status')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible" role="alert">
            <div class="alert-message">
            Login terlebih dahulu!
            </div>
        </div>');
            redirect('auth/login');
        }
        
    }

    public function index()
    {
        $data = [
            'title' => 'Setting',
            'settings' => $this->setting_model->get_settings()
        ];
        $this->load->view('/setting/index', $data);
    }

    public function update()
    {

        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('alamat', 'Alamat', 'required');
        $this->form_validation->set_rules('receipt_message', 'Pesan Tanda Terima', 'required');
        if ($this->form_validation->run() == false) {
            $this->index();
        } else {
            $this->setting_model->update($this->input->post('id_receipt_message'), [
                'desc' => $this->input->post('receipt_message'),
            ]);
            $this->setting_model->update($this->input->post('id_alamat'), [
                'desc' => $this->input->post('alamat'),
            ]);
            $this->setting_model->update($this->input->post('id_nama'), [
                'desc' => $this->input->post('nama'),
            ]);
            $this->session->set_flashdata('success', 'Setting Berhasil Diperbarui!');
            redirect('/setting');
        }
    }
}
