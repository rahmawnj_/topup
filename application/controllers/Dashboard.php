<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model(['log_model']);
    }

    public function index()
    {
        $data['title'] = 'Dashboard';
        $this->load->view('index', $data);
    }
}
