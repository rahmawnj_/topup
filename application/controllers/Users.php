<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Users extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library("form_validation");
        $this->load->model(['user_model', 'setting_model']);
        $this->load->helper(['form', 'url']);
        if (!$this->session->userdata('status')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible" role="alert">
            <div class="alert-message">
            Login terlebih dahulu!
            </div>
        </div>');
            redirect('auth/login');
        }
    }

    public function index()
    {
        
        $data = [
            'title' => 'Users',
            'users' => $this->user_model->get_users()
        ];
        $this->load->view('users/index', $data);
    }

    public function create()
    {
        $data = [
            'title' => 'Users',
        ];
        $this->load->view('users/create', $data);
    }

    public function store()
    {
        $this->form_validation->set_rules('name', 'Name', 'required|trim');
        $this->form_validation->set_rules('username', 'Username', 'required|trim|is_unique[users.username]');
        $this->form_validation->set_rules('password', 'Password', 'required|trim|min_length[5]|max_length[12]');
        if ($this->form_validation->run() == false) {
            $this->create();
        } else {

            $config['upload_path']          = './assets/img/uploads/users';
            $config['allowed_types']        = 'gif|jpg|png|jpeg';
            $config['max_size']             = 10000;
            $config['max_width']            = 10000;
            $config['max_height']           = 10000;

            $this->load->library('upload', $config);
            $this->upload->do_upload('photo');
            $this->user_model->insert([
                'name' => $this->input->post('name'),
                'username' => $this->input->post('username'),
                'photo' => 'users/' . $this->upload->data('file_name'),
                'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
                'created_at' => date('Y-m-d H:i:s')
            ]);
            $this->session->set_flashdata('success', 'User Berhasil Ditambahkan!');
            redirect('/users');
        }
    }

    public function view($id_user)
    {
        $data = [
            'title' => 'Users',
            'user' => $this->user_model->get_user('id_user', $id_user)[0]
        ];
        $this->load->view('users/view', $data);
    }

    public function edit($id_user)
    {
        $data = [
            'title' => 'Users',
            'user' => $this->user_model->get_user('id_user', $id_user)[0]
        ];
        $this->load->view('users/edit', $data);
    }

    public function update()
    {
        $user = $this->user_model->get_user('id_user', $this->input->post('id_user'))[0];
        if ($this->input->post('username') == $user['username']) {
            $username_rules = 'required|trim';
        } else {
            $username_rules = 'required|trim|is_unique[users.username]';
        }

        $this->form_validation->set_rules('name', 'Name', 'required|trim');
        $this->form_validation->set_rules('username', 'Username', $username_rules);

        if ($this->form_validation->run() == false) {
            $this->edit($this->input->post('id_user'));
        } else {
            $config['upload_path']          = './assets/img/uploads/users';
            $config['allowed_types']        = 'gif|jpg|png|jpeg';
            $config['max_size']             = 10000;
            $config['max_width']            = 10000;
            $config['max_height']           = 10000;

            $this->load->library('upload', $config);
            $this->upload->do_upload('photo');

            if ($this->upload->data('file_name') == '') {
                $photo = $user['photo'];
            } else {
                unlink('assets/img/uploads/' . $user['photo']);
                $photo = 'users/' . $this->upload->data('file_name');
            }
            if ($this->input->post('username') == $this->session->userdata('username')) {
             
                $this->session->set_userdata([
                    'photo' => $photo,
                ]);
            } 

            if ($this->input->post('password') == '') {
                $password = $user['password'];
            } else {
                $password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
            }

            $this->user_model->update($this->input->post('id_user'), [
                'name' => $this->input->post('name'),
                'username' => $this->input->post('username'),
                'photo' => $photo,
                'password' => $password,
                'updated_at' => date('Y-m-d H:i:s')
            ]);

            $this->session->set_flashdata('success', 'User Berhasil Diperbarui!');
            redirect('/users');
        }
    }

    public function delete($id_user)
    {
        
        $user = $this->user_model->get_user('id_user', $id_user)[0];

        $this->user_model->delete($id_user);
        unlink('assets/img/uploads/' . $user['photo']);
        $this->session->set_flashdata('success', 'User Berhasil Dihapus!');
        redirect('/users');
    }
}

