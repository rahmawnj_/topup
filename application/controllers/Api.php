<?php
defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Api extends RestController
{
    function __construct()
    {
        parent::__construct();
        $this->load->library("form_validation");
        $this->load->model(['device_model', 'log_model']);
    }

    public function insert_topup_post()
    {
        if (is_null($this->input->post('id_device'))) {
            $this->response(['msg' => 'id_device perlu dikirim', 'status' => 404]);
        }
        if (is_null($this->input->post('nominal'))) {
            $this->response(['msg' => 'nominal perlu dikirim', 'status' => 404]);
        }
        if (is_null($this->input->post('status'))) {
            $this->response(['msg' => 'status perlu dikirim', 'status' => 404]);
        }
        $id_device = $this->device_model->get_device('code', $this->input->post('id_device'));
        if ($id_device) {
            $data = [
                'id_device' => $this->input->post('id_device'),
                'status' => $this->input->post('status'),
                'nominal' => $this->input->post('nominal'),
                'created_at' => date('Y-m-d H:i:s')
            ];
            $this->log_model->insert($data);

            $this->db->select("*");
            $this->db->from("log");
            $this->db->limit(1);
            $this->db->order_by('id_log', 'desc');
            $query = $this->db->get();
            $id_log = $query->result_array()[0]['id_log'];

            $this->response(['msg' => 'insert topup', 'id_log' => $id_log, 'status' => 200]);
        } else {
            $this->response(['msg' => 'id_device tidak ditemukan', 'status' => 404]);
        }
    }
    public function update_topup_post()
    {
        if (is_null($this->input->post('id_log'))) {
            $this->response(['msg' => 'id_log perlu dikirim', 'status' => 404]);
        }
        if (is_null($this->input->post('status'))) {
            $this->response(['msg' => 'status perlu dikirim', 'status' => 404]);
        }
        $id_log = $this->log_model->get_log('id_log', $this->input->post('id_log'));
        if ($id_log) {
            $this->log_model->update($this->input->post('id_log'), [
                'status' => $this->input->post('status'),
                'updated_at' => date('Y-m-d H:i:s')
            ]);
            $this->response(['msg' => 'update topup', 'status_topup' => $this->input->post('status'), 'status' => 200]);
        } else {
            $this->response(['msg' => 'id_log tidak ditemukan', 'status' => 404]);
        }
    }
}
