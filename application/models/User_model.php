<?php
class User_model extends CI_Model
{
    public function get_users()
    {
        return $this->db->get('users')->result_array();
    }

    public function get_user($field, $data)
    {
        return $this->db->get_where('users', [$field => $data])->result_array();
    }

    public function insert(...$data)
    {
        $this->db->insert('users', $data[0]);
    }

    public function update($id_user, ...$data)
    {
        $this->db->update('users', $data[0], ['id_user' => $id_user]);
    }

    public function delete($id_user)
    {
        $this->db->delete('users', array('id_user' => $id_user));
    }
}
