<?php
class Setting_model extends CI_Model
{
    public function get_settings()
    {
        return $this->db->get('setting')->result_array();
    }

    public function get_setting($field, $data)
    {
        return $this->db->get_where('setting', [$field => $data])->result_array();
    }

    public function update($id_setting, ...$data)
    {
        return $this->db->update('setting', $data[0], ['id_setting' => $id_setting]);
    }

}

