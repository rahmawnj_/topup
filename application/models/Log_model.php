<?php
class Log_model extends CI_Model
{
    public function get_logs()
    {
        $this->db->select('devices.code as device_code, devices.name as device_name, log.*');
        $this->db->join('devices', 'devices.code = log.id_device');
        $this->db->order_by('log.created_at', 'desc');
        return $this->db->get('log')->result_array();
    }
    public function get_log_by_date($date)
    { 
        $this->db->select('devices.code as device_code, devices.name as device_name, log.*');
        $this->db->join('devices', 'devices.code = log.id_device');
        $this->db->order_by('log.created_at', 'desc');
        return $this->db->query("SELECT * FROM log WHERE DATE(created_at) = '$date'")->result_array();
    }

    public function get_log($field, $data)
    {
        return $this->db->get_where('log', [$field => $data])->result_array();
    }

    public function insert(...$data)
    {
        return $this->db->insert('log', $data[0]);
    }

    public function update($id_log, ...$data)
    {
        return $this->db->update('log', $data[0], ['id_log' => $id_log]);
    }

    public function delete($id_log)
    {
        $this->db->where('id_log', $id_log);
        return $this->db->delete('log');
    }
}
