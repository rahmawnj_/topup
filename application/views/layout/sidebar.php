<div id="sidebar" class="app-sidebar" style="background-color: aliceblue;">
    <!-- BEGIN scrollbar -->
    <div class="app-sidebar-content" data-scrollbar="true" data-height="100%">
        <!-- BEGIN menu -->
        <div class="menu">
            <div class="menu-profile">
                <a href="javascript:;" class="menu-profile-link" data-toggle="app-sidebar-profile" data-target="#appSidebarProfileMenu">
                    <div class="menu-profile-cover with-shadow"></div>
                    <div class="menu-profile-image">
                        <img src="<?= base_url('/assets/img/uploads/' . $this->session->userdata('photo')) ?>" alt="" />
                    </div>
                    <div class="menu-profile-info">
                        <div class="d-flex align-items-center">
                            <div class="flex-grow-1">
                                <?= $this->session->userdata('name') ?>
                            </div>
                            <div class="menu-caret ms-auto"></div>
                        </div>
                        <small><?= $this->session->userdata('username') ?></small>
                    </div>
                </a>
            </div>
            <div id="appSidebarProfileMenu" class="collapse">

                <div class="menu-item pb-5px <?= ($title == 'Setting') ? 'active' : '' ?>">
                    <a href="<?= base_url('setting') ?>" g class="menu-link">
                        <div class="menu-icon"><i class="fa fa-cog"></i></div>
                        <div class="menu-text"> Setting</div>
                    </a>
                </div>
                <div class="menu-item pb-5px">
                    <a href="<?= base_url('auth/logout') ?>" g class="menu-link">
                        <div class="menu-icon"><i class="fa fa-sign-out-alt"></i></div>
                        <div class="menu-text"> Logout</div>
                    </a>
                </div>
                <div class="menu-divider m-0"></div>
            </div>
            <div class="menu-header">Navigation</div>
            <div class="menu-item <?= ($title == 'Users') ? 'active' : '' ?>">
                <a href="<?= base_url('/users') ?>" class="menu-link">
                    <div class="menu-icon">
                        <i class="fa fa-user"></i>
                    </div>
                    <div class="menu-text">User</div>
                </a>
            </div>
            <div class="menu-item <?= ($title == 'Devices') ? 'active' : '' ?>">
                <a href="<?= base_url('/devices') ?>" class="menu-link">
                    <div class="menu-icon">
                        <i class="fa fa-mobile"></i>
                    </div>
                    <div class="menu-text">Device</div>
                </a>
            </div>
            <div class="menu-item <?= ($title == 'Transaksi Topup') ? 'active' : '' ?>">
                <a href="<?= base_url('/logs') ?>" class="menu-link">
                    <div class="menu-icon">
                        <i class="fa fa-money-bill-alt"></i>
                    </div>
                    <div class="menu-text">Transaksi Top Up</div>
                </a>
            </div>
            <!-- <div class="menu-item <?= ($title == 'Dump Devices') ? 'active' : '' ?> has-sub">
                <a href="javascript:;" class="menu-link">
                    <div class="menu-icon">
                        <i class="fa fa-trash"></i>
                    </div>
                    <div class="menu-text">Dump</div>
                    <div class="menu-caret"></div>
                </a>
                <div class="menu-submenu">
                    <div class="menu-item <?= ($title == 'Dump Devices') ? 'active' : '' ?>">
                        <a href="<?= base_url('/devices/dump') ?>" class="menu-link">
                            <div class="menu-text">Dump Devices</div>
                        </a>
                    </div>
                </div>
            </div> -->



            <!-- BEGIN minify-button -->
            <div class="menu-item d-flex">
                <a href="javascript:;" class="app-sidebar-minify-btn ms-auto" data-toggle="app-sidebar-minify"><i class="fa fa-angle-double-left"></i></a>
            </div>
            <!-- END minify-button -->
        </div>
        <!-- END menu -->
    </div>
    <!-- END scrollbar -->
</div>
<div class="app-sidebar-bg"></div>
<div class="app-sidebar-mobile-backdrop"><a href="#" data-dismiss="app-sidebar-mobile" class="stretched-link"></a></div>
<!-- END #sidebar -->