<div id="header" class="app-header app-header-inverse">
			<!-- BEGIN navbar-header -->
			<div class="navbar-header">
				<a href="#" class="navbar-brand"><i class="fa fa-credit-card fa-lg"></i> <?php
				$CI =&get_instance();
                                $CI->load->model('setting_model');
                                echo $CI->setting_model->get_setting('name', 'name')[0]['desc']?></a>
				<button type="button" class="navbar-mobile-toggler" data-toggle="app-sidebar-mobile">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>
			<!-- END navbar-header -->
			<!-- BEGIN header-nav -->
			<div class="navbar-nav">
			
				<div class="navbar-item navbar-user dropdown">
					<a href="#" class="navbar-link dropdown-toggle d-flex align-items-center" data-bs-toggle="dropdown">
						<img src="<?= base_url('/assets/img/uploads/' . $this->session->userdata('photo')) ?>" alt="" />
						<span class="d-none d-md-inline"><?= $this->session->userdata('name') ?></span> <b class="caret ms-6px"></b>
					</a>
					<div class="dropdown-menu dropdown-menu-end me-1">
						<a href="<?= base_url('setting') ?>" class="dropdown-item">Setting</a>
						<a href="<?= base_url('auth/logout') ?>" class="dropdown-item">Log Out</a>
					</div>
				</div>
			</div>
			<!-- END header-nav -->
		</div>