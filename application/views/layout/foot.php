
	<!-- ================== BEGIN core-js ================== -->
	<script src="<?= base_url('assets/js/vendor.min.js') ?>"></script>
	<script src="<?= base_url('assets/js/app.min.js') ?>"></script>
	<script src="<?= base_url('assets/js/theme/facebook.min.js') ?>"></script>
	<!-- ================== END core-js ================== -->

	<!-- ================== BEGIN page-js ================== -->
	<script src="<?= base_url('assets/plugins/datatables.net/js/jquery.dataTables.min.js')?>"></script>
	<script src="<?= base_url('assets/plugins/datatables.net-bs4/js/dataTables.bootstrap4.min.js')?>"></script>
	<script src="<?= base_url('assets/plugins/datatables.net-responsive/js/dataTables.responsive.min.js')?>"></script>
	<script src="<?= base_url('assets/plugins/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js')?>"></script>
	<script src="<?= base_url('assets/plugins/datatables.net-buttons/js/dataTables.buttons.min.js')?>"></script>
	<script src="<?= base_url('assets/plugins/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js')?>"></script>
	<script src="<?= base_url('assets/plugins/datatables.net-buttons/js/buttons.colVis.min.js')?>"></script>
	<script src="<?= base_url('assets/plugins/datatables.net-buttons/js/buttons.flash.min.js')?>"></script>
	<script src="<?= base_url('assets/plugins/datatables.net-buttons/js/buttons.html5.min.js')?>"></script>
	<script src="<?= base_url('assets/plugins/datatables.net-buttons/js/buttons.print.min.js')?>"></script>
	<script src="<?= base_url('assets/plugins/pdfmake/build/pdfmake.min.js')?>"></script>
	<script src="<?= base_url('assets/plugins/pdfmake/build/vfs_fonts.js')?>"></script>
	<script src="<?= base_url('assets/plugins/jszip/dist/jszip.min.js')?>"></script>
	<script src="<?= base_url('assets/js/demo/table-manage-buttons.demo.js')?>"></script>
	<script src="<?= base_url('assets/plugins/@highlightjs/cdn-assets/highlight.min.js')?>"></script>
	<script src="<?= base_url('assets/js/demo/render.highlight.js')?>"></script>

	<script src="<?= base_url('/assets/plugins/gritter/js/jquery.gritter.js')?>"></script>
	<script src="<?= base_url('/assets/plugins/sweetalert/dist/sweetalert.min.js')?>"></script>
	<script src="<?= base_url('/assets/js/demo/ui-modal-notification.demo.js')?>"></script>
	<script src="<?= base_url('/assets/plugins/@highlightjs/cdn-assets/highlight.min.js')?>"></script>
	<script src="<?= base_url('/assets/js/demo/render.highlight.js')?>"></script>
	<!-- ================== END page-js ================== -->
	<script>
		const gambar = document.getElementById('gambar')
		$(gambar).change(evt => {
			const [file] = gambar.files
			if (file) {
				gmbr.src = URL.createObjectURL(file)
			}
		})

	
		const flashDataError = $('.flash-data-error').data('flashdataerror')
		if (flashDataError) {
			swal({
				title: "Gagal!",
				text: flashDataError,
				icon: "error",
			});
		}

		const flashDataSuccess = $('.flash-data-success').data('flashdatasuccess')
		if (flashDataSuccess) {
			swal({
				title: "Berhasil!",
				text: flashDataSuccess,
				icon: "success",
			});
		}

		const flashDataWarning = $('.flash-data-warning').data('flashdatawarning')
		if (flashDataWarning) {
			swal({
				title: "Peringatan!",
				text: flashDataWarning,
				icon: "warning",
			});
		}

		$('#data-table-default').DataTable({
			responsive: true,
			dom: '<"row"<"col-sm-5"B><"col-sm-7"fr>>t<"row"<"col-sm-5"i><"col-sm-7"p>>',
			buttons: [
			{ extend: 'copy', className: 'btn-sm' },
			{ extend: 'csv', className: 'btn-sm' },
			{ extend: 'excel', className: 'btn-sm' },
			{ extend: 'pdf', className: 'btn-sm' },
			{ extend: 'print', className: 'btn-sm' }
			],
		});
	</script>
</body>

</html>