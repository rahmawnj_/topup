<?php $this->load->view('layout/head') ?>

<div id="app" class="app app-header-fixed app-sidebar-fixed">
    <!-- END #header -->
    <?php $this->load->view('layout/header') ?>
    <!-- BEGIN #sidebar -->
    <?php $this->load->view('layout/sidebar') ?>

    <div id="content" class="app-content">
        <!-- BEGIN breadcrumb -->
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
            <li class="breadcrumb-item"><a href="javascript:;"><?= $title ?></a></li>
        </ol>
        <!-- END breadcrumb -->
        <!-- BEGIN page-header -->
        <!-- END page-header -->
        <!-- BEGIN row -->
        <div class="row">
            <!-- BEGIN col-2 -->

            <!-- END col-2 -->
            <!-- BEGIN col-10 -->
            <div class="col-xl-12">
                <!-- BEGIN panel -->
                <div class="panel panel-inverse">
                    <!-- BEGIN panel-heading -->
                    <div class="panel-heading">
                        <h4 class="panel-title"><?= $title ?></h4>
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-default" data-toggle="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-success" data-toggle="panel-reload"><i class="fa fa-redo"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-warning" data-toggle="panel-collapse"><i class="fa fa-minus"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-danger" data-toggle="panel-remove"><i class="fa fa-times"></i></a>
                        </div>
                    </div>
                    <!-- END panel-heading -->


                    <!-- END alert -->
                    <!-- BEGIN panel-body -->
                    <?= $this->session->flashdata('message'); ?>

                    <div class="panel-body">
                        <form method="post" action="<?= base_url('setting/update') ?>">
                            <fieldset>

                                <div class="mb-3">
                                    <input type="hidden" class="form-control" name="id_nama" value="<?= $settings[0]['id_setting'] ?>">
                                    <label class="form-label" for="nama">Nama</label>
                                    <input required autocomplete="off" value="<?= $settings[0]['desc'] ?>" type="text" class="form-control" name="nama" id="nama" aria-describedby="nama" placeholder="ID Device" required>
                                    <span class="text-danger">
                                        <?= form_error('nama') ?>
                                    </span>
                                </div>


                                <div class="mb-3">
                                    <input type="hidden" class="form-control" name="id_alamat" value="<?= $settings[1]['id_setting'] ?>">
                                    <label class="form-label" for="alamat">Alamat</label>
                                    <textarea required name="alamat" class="form-control" id="" rows="5"><?= $settings[1]['desc'] ?></textarea>
                                    <span class="text-danger">
                                        <?= form_error('alamat') ?>
                                    </span>
                                </div>
                                <div class="mb-3">
                                    <input type="hidden" class="form-control" name="id_receipt_message" value="<?= $settings[2]['id_setting'] ?>">
                                    <label class="form-label" for="receipt_message">Pesan Tanda Terima</label>
                                    <textarea required name="receipt_message" class="form-control" id="" rows="5"><?= $settings[2]['desc'] ?></textarea>
                                    <span class="text-danger">
                                        <?= form_error('receipt_message') ?>
                                    </span>
                                </div>
                                <button type="submit" class="btn btn-primary w-100px me-5px">Submit</button>
                            </fieldset>
                        </form>
                    </div>

                    <!-- END hljs-wrapper -->
                </div>
                <!-- END panel -->
            </div>
            <!-- END col-10 -->
        </div>
        <!-- END row -->
    </div>
    <!-- END #content -->

</div>

<?php $this->load->view('layout/foot') ?>