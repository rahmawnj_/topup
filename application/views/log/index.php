<?php $this->load->view('layout/head') ?>

<div id="app" class="app app-header-fixed app-sidebar-fixed">
    <!-- END #header -->
    <?php $this->load->view('layout/header') ?>
    <!-- BEGIN #sidebar -->
    <?php $this->load->view('layout/sidebar') ?>

    <div id="content" class="app-content">
        <!-- BEGIN breadcrumb -->
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
            <li class="breadcrumb-item"><a href="javascript:;"><?= $title ?></a></li>
        </ol>
        <!-- END breadcrumb -->
        <!-- BEGIN page-header -->
        <!-- END page-header -->
        <!-- BEGIN row -->
        <div class="row">
            <!-- BEGIN col-2 -->

            <!-- END col-2 -->
            <!-- BEGIN col-10 -->
            <div class="col-xl-12">
                <!-- BEGIN panel -->
                <div class="panel panel-inverse">
                    <!-- BEGIN panel-heading -->
                    <div class="panel-heading">
                        <h4 class="panel-title"><?= $title ?></h4>
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-default" data-toggle="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-success" data-toggle="panel-reload"><i class="fa fa-redo"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-warning" data-toggle="panel-collapse"><i class="fa fa-minus"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-danger" data-toggle="panel-remove"><i class="fa fa-times"></i></a>
                        </div>
                    </div>


                    <div class="flash-data-success" data-flashdatasuccess="<?= $this->session->flashdata('success') ?>"></div>
                    <div class="flash-data-error" data-flashdataerror="<?= $this->session->flashdata('error') ?>"></div>

                    <div class="panel-body" class="table-responsive">

                        <table style="width: 100%;" id="data-table-default" class="table table-striped table-bordered align-middle">
                            <thead>
                                <tr>
                                    <th width="1%"></th>
                                    <th class="text-nowrap">ID Log</th>
                                    <th class="text-nowrap">ID Device</th>
                                    <th class="text-nowrap">Nominal</th>
                                    <th class="text-nowrap">Status</th>
                                    <th class="text-nowrap">Insert</th>
                                    <th class="text-nowrap">Update</th>
                                    <th class="text-nowrap"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 0;
                                foreach ($logs as $log) : ?>
                                    <tr>
                                        <td width="1%"><?= ++$no; ?></td>
                                        <td><?= $log['id_log'] ?></td>
                                        <td><?= $log['device_code'] ?> - <?= $log['device_name'] ?> </td>
                                        <td><?= number_format($log['nominal'], 3, '.', '.') ?></td>
                                        <td><?= ($log['status'] == '1') ? 'terkonfirmasi' : 'belum dikonfirmasi' ?></td>
                                        <td><?= $log['created_at'] ?></td>
                                        <td><?= $log['updated_at'] ?></td>
                                        <td>
                                            <?php if ($log['status'] == '1') : ?>
                                                <button onclick="printTopup('topup-transaction-<?= $log['id_log'] ?>')" class="btn btn-sm btn-dark"><i class="fa fa-print"></i></button>
                                            <?php endif ?>
                                            <br>
                                            <div style="display: none; " id="topup-transaction-<?= $log['id_log'] ?>">
                                                 <div class="" style="display:flex; justify-content:center; ">
                                                    <!DOCTYPE html>
                                                    <html>
                                                    <head>
                                                    </head>
                                                    <body style="">
                                                        <div style="border: 1px solid black; padding:20px 15px; margin:auto; width: 20cm;">
                                                            <div style="font-weight: bolder; font-size: 0.5cm; margin-bottom:2px;" class="text-center">
                                                                <?= $settings[0]['desc'] ?>
                                                            </div>
                                                            <div style="font-size: 0.3cm; margin-bottom:2px;" class="text-center">
                                                                <?= $settings[1]['desc'] ?>
                                                            </div>
                                                            <div class="mt-3" style="">
                                                                <p style="font-size:0.3cm;">
                                                                    <b>
                                                                        Tanggal
                                                                    </b>
                                                                    <?= date_format(date_create($log['created_at']), "d-m-Y H:i:s") ?>
                                                                </p>
                                                            </div>
                                                            <div class="mb-3 ">
                                                                <span style="font-weight: bold; font-size:0.3cm;">
                                                                    <?= $this->session->userdata('name') ?>
                                                                </span>
                                                            </div>
                                                            <div class="" style="font-size:0.3cm;">
                                                                <b>Struk Pembelian</b> <br>
                                                                <span>Total : <?= "Rp" . number_format($log['nominal'], 3, '.', '.') ?></span> <br>
                                                                <div class="text-center mt-2" style="font-size:0.3cm;">
                                                                    <p style="text-align-last:center;"><?= $settings[2]['desc'] ?></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </body>

                                                    </html>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>

                </div>
                <!-- END panel -->
            </div>
            <!-- END col-10 -->
        </div>
        <!-- END row -->
    </div>
    <!-- END #content -->

</div>
<script>
    function printTopup(divName) {
        console.log(divName)
        var printContents = document.getElementById(divName).innerHTML
        var originalContents = document.body.innerHTML
        document.body.innerHTML = printContents
        window.print()
        console.log(divName)

        document.body.innerHTML = originalContents
    }
</script>
<?php $this->load->view('layout/foot') ?>